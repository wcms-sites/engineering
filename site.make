core = 7.x
api = 2

; webform_remote_post
projects[webform_remote_post][type] = "module"
projects[webform_remote_post][download][type] = "git"
projects[webform_remote_post][download][url] = "https://git.uwaterloo.ca/drupal-org/webform_remote_post.git"
projects[webform_remote_post][download][tag] = "7.x-1.2"

; uw_faculty_selector
projects[uw_faculty_selector][type] = "module"
projects[uw_faculty_selector][download][type] = "git"
projects[uw_faculty_selector][download][url] = "https://git.uwaterloo.ca/wcms/uw_faculty_selector.git"
projects[uw_faculty_selector][download][tag] = "7.x-1.0"

; uw_webform_to_sf
projects[uw_webform_to_sf][type] = "module"
projects[uw_webform_to_sf][download][type] = "git"
projects[uw_webform_to_sf][download][url] = "https://git.uwaterloo.ca/wcms/uw_webform_to_sf.git"
projects[uw_webform_to_sf][download][tag] = "7.x-1.1"

; uw_webform_webhook
projects[uw_webform_webhook][type] = "module"
projects[uw_webform_webhook][download][type] = "git"
projects[uw_webform_webhook][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_webform_webhook.git"
projects[uw_webform_webhook][download][tag] = "7.x-1.0"
projects[uw_webform_webhook][subdir] = ""
